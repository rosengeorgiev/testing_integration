# README #


### What is this repository for? ###

* This repo is for test automation framework with java for : UI, Rest API , Android , iOS and Windows


### How do I get set up? ###

* Install Java > 8 - https://www.java.com/en/download/help/download_options.xml
* Install gradle - https://gradle.org/install/
* Favorite IDE (I recommend IntelliJ IDEA - https://www.jetbrains.com/idea/)
* If using IntelliJ - Install Cucumber for Java & Gherkin addons
* For Android & iOS testing we need Appium - https://github.com/appium/appium-desktop/
* Run configuration for local example:
                               -Ptags=@test,~@wip,~@android
                               -Penv="local"
                               -PchromeDriver=C:\drivers\chromedriver.exe

### Who do I talk to? ###

*PS. Hire me !
* rosenkirchev@gmail.com