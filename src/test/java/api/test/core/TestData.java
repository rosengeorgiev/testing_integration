package api.test.core;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;


public class TestData {

    public static final Set<String> listOfdefaultLocales = new HashSet<>(Arrays.asList("en", "de", "ja", "es", "bg", "hi", "en-US", "en-AU", "de-AT", "de-DE"));
    public static final Set<String> listOftimeZones = new HashSet<>(Arrays.asList("Africa/Accra", "Africa/Addis_Ababa", "Africa/Algiers", "America/Argentina/Cordoba",
            "America/Argentina/Rio_Gallegos", "America/Aruba", "America/Cancun", "America/Indiana/Marengo", "America/Los_Angeles", "America/Santiago", "Antarctica/Davis",
            "Asia/Amman", "Asia/Hong_Kong", "Asia/Tokyo", "Australia/Darwin", "Europe/Amsterdam", "Europe/Helsinki"));

}
