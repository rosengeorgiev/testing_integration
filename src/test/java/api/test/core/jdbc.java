package api.test.core;

import api.test.rest.RestSessionContainer;
import core.JsonUtils;

import java.sql.*;
import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.SQLException;

public class jdbc {
    RestSessionContainer restSessionContainer;

    public jdbc(RestSessionContainer restSessionContainer) {
        this.restSessionContainer = restSessionContainer;
    }

// JDBC driver name and database URL
    static String JDBC_DRIVER = "org.postgresql.Driver";
    static String DB_URL = "";

//Database credentials
    static String USER = "";
    static String PASS = "";

    public void getActivationCodeFromTheDB() {
        Connection conn = null;
        Statement stmt = null;

        try {
//STEP 2: Register JDBC driver
            Class.forName("org.postgresql.Driver");
//STEP 3: Open a connection
            System.out.println("Connecting to database…");
            conn = DriverManager.getConnection(
                    "jdbc:postgresql://"+DB_URL,USER, PASS);
//STEP 4: Execute a query
            System.out.println("Creating statement");
            stmt = conn.createStatement();
            String sql;
            sql = "";
            ResultSet rs = stmt.executeQuery(sql);

//STEP 5: Extract data from result set
            while (rs.next()) {
            }

//STEP 6: Clean-up environment
            rs.close();
            stmt.close();
            conn.close();
        } catch (SQLException se) {
//Handle errors for JDBC
            se.printStackTrace();
        } catch (Exception e) {
//Handle errors for Class.forName
            e.printStackTrace();
        } finally {
            try {
                if (conn != null)
                    conn.close();
            } catch (SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
    }//end main
}//end FirstExample