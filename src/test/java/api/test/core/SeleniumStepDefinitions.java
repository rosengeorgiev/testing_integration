package api.test.core;

import cucumber.api.java.en.And;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.Augmenter;
import selenium.utils.Browser;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.LinkedHashMap;
import java.util.Map;

import static selenium.utils.Browser.driver;

public class SeleniumStepDefinitions extends BaseStepDefinitions {
    private static final Logger logger = LogManager.getLogger(SeleniumStepDefinitions.class.getSimpleName());

    public static void takeScreenShot() throws IOException {
        String filename = "screenshot.png";
        String workingDirectory = System.getProperty("user.dir");
        String absoluteFilePath;

        absoluteFilePath = workingDirectory + File.separator + filename;
        WebDriver augmentedDriver = new Augmenter().augment(driver);
        File screenshot = ((TakesScreenshot)augmentedDriver).
                getScreenshotAs(OutputType.FILE);
        FileUtils.copyFile(screenshot, new File(absoluteFilePath));
    }

    public static Map<String, String> splitUrl(URL url) throws UnsupportedEncodingException {
        Map<String, String> query_pairs = new LinkedHashMap<String, String>();
        String query = url.getQuery();
        String[] pairs = query.split("&");
        for (String pair : pairs) {
            int idx = pair.indexOf("=");
            query_pairs.put(URLDecoder.decode(pair.substring(0, idx), "UTF-8"), URLDecoder.decode(pair.substring(idx + 1), "UTF-8"));
        }
        return query_pairs;
    }

    @And("^I close the browser$")
    public void iCloseTheBrowser() throws Throwable {
        Browser.driver.manage().deleteAllCookies();
        Browser.tearDown();
        logger.debug("BROWSER CLOSED");
    }

    /**
     *
     * @param browser - acceptable values are FireFox, Chrome, HTMLUnitDriver
     * @throws Throwable
     */
    @And("^I start the browser \\{(.*?)\\}$")
    public void iStartTheBrowser(String browser) throws Throwable {
        switch(browser.toLowerCase()) {
            case "firefox":
                Browser.initFF();
                break;
            case "chrome":
                Browser.initChrome();
                break;
            case "htmlunitdriver":
                Browser.initHTMLUnitDriver();
                break;
            default:
                throw new IllegalArgumentException("Invalid browser: " + browser);
        }
        logger.debug("BROWSER STARTED: {}" , browser);
    }

    @And("^I resize the browser to width \\{(.*?)\\} and height \\{(.*?)\\}$")
    public void iResizeTheBrowser(int width, int height) throws Throwable {
        Dimension dim = new Dimension(width,height);
        driver.manage().window().setSize(dim);
    }

}
