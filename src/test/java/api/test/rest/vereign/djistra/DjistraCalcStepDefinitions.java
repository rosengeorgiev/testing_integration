package api.test.rest.vereign.djistra;

import api.test.core.BaseStepDefinitions;
import api.test.rest.RestSessionContainer;
import com.github.fge.jackson.JsonLoader;
import com.google.gson.*;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.ReadContext;
import core.*;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import exceptions.RAFException;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import utils.RAFRandomize;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class DjistraCalcStepDefinitions extends BaseStepDefinitions {
    private static final Logger logger = LogManager.getLogger(DjistraCalcStepDefinitions.class.getSimpleName());
    RestSessionContainer restSessionContainer;
    Request currentRequest;

    public DjistraCalcStepDefinitions(RestSessionContainer restSessionContainer, Request currentRequest, DataContainer dataContainer) {
        super(dataContainer);
        this.restSessionContainer = restSessionContainer;
        this.currentRequest = currentRequest;
    }

    @Then("^I make a request to calculate a new distance with Vereign Djistra api$")
    public void i_make_a_request_to_calculate_a_new_distance_with_Vereign_Djistra_api() throws Throwable {
        currentRequest.setPath("/1/calc");

        Response response = RestClient.post(currentRequest);
        addRequest(currentRequest);
        addResponse(response);

//        if (response.getStatusCode() == 201) {
//
//        }
    }

    @Then("^I choose a random destinations for From min node \\{(.*)\\} max node \\{(.*)\\} and To min node \\{(.*)\\} max node \\{(.*)\\} and calculate the distance with Vereign Djistra api$")
    public void I_choose_a_random_destinations_for_From_and_To_and_calculate_the_distance_with_Vereign_Djistra_api(int fromMin,int fromMax,int toMin, int toMax) throws Throwable {
        Gson gson = new Gson();
        JsonElement initialBody = gson.fromJson(currentRequest.getBody(), JsonElement.class);
        // only try to load body as JsonObject if it is not null.
        JsonObject newBody = new JsonObject();
        if (initialBody != null) newBody = initialBody.getAsJsonObject();
        newBody.addProperty("from", RAFRandomize.getInt(fromMin,fromMax));
        newBody.addProperty("to", RAFRandomize.getInt(toMin,toMax));
        newBody.addProperty("threads", 10);
        currentRequest.setBody(gson.toJson(newBody));

        i_make_a_request_to_calculate_a_new_distance_with_Vereign_Djistra_api();
    }

 }