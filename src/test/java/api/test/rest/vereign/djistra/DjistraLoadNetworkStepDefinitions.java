package api.test.rest.vereign.djistra;

import api.test.core.BaseStepDefinitions;
import api.test.rest.RestSessionContainer;
import core.DataContainer;
import core.Request;
import core.Response;
import core.RestClient;
import cucumber.api.java.en.Then;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DjistraLoadNetworkStepDefinitions extends BaseStepDefinitions {
    private static final Logger logger = LogManager.getLogger(DjistraLoadNetworkStepDefinitions.class.getSimpleName());
    RestSessionContainer restSessionContainer;
    Request currentRequest;

    public DjistraLoadNetworkStepDefinitions(RestSessionContainer restSessionContainer, Request currentRequest, DataContainer dataContainer) {
        super(dataContainer);
        this.restSessionContainer = restSessionContainer;
        this.currentRequest = currentRequest;
    }

    @Then("^I make a request to Load New Networks with Vereign Djistra api$")
    public void I_make_a_request_to_Load_New_Networks_with_Vereign_Djistra_api() throws Throwable {
        currentRequest.setPath("/1/loadnetwork");

        Response response = RestClient.post(currentRequest);
        addRequest(currentRequest);
        addResponse(response);
    }

 }