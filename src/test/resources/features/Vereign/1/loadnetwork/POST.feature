#/1/loadnetwork
#Author: Rosen Georgiev rosen.georgiev@sumup.com

@vereign @rest
Feature: Vereign - 1 - loadnetwork POST
  This method loads new network into user graph structure

  Background:
    Given we are testing the Vereign Djistra Api

  @loadNetwork
  Scenario: Load a new network - [load_3_edges] - Positive
    Given I load the REST request {LoadNetwork.json} with profile {load_3_edges}
    Then I make a request to Load New Networks with Vereign Djistra api
    Then the status code should be {201}
    And the response is valid according to the {VereignDjistra.json} REST schema
    And the field {$.result} has the value {Graph loaded for user john}
    And the field {$.errors} has the value {none}

  ##cURL of this test returns : curl: (52) Empty reply from server
  ##since the invalid tests are not handled at the moment this negative tests shall be commented out with tag @wip (work in progress)
  ##This is how some of the negative tests may look like
  @loadNetwork @negative @wip
  Scenario Outline: Load a new with missing required params - [<param>] - Negative
    Given I load the REST request {LoadNetwork.json} with profile {load_3_edges}
    Then I remove the following Keys from current request Body:
      | <param> |
    Then I make a request to Load New Networks with Vereign Djistra api
    Then the status code should be {201}
    And the response is valid according to the {VereignDjistra.json} REST schema
    And the field {$.result} has the value {???}
    And the field {$.errors} has the value {none}
    Examples:
      | param     |
      | edges     |
      | nodes     |
      | edgesList |