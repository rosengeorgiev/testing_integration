#/1/calc
#Author: Rosen Georgiev rosen.georgiev@sumup.com

@vereign @rest
Feature: Vereign - 1 - calc POST
  This method calculates distance between two given cities and starts Dijkstra algorithm on multiple threads

  Background:
    Given we are testing the Vereign Djistra Api

  @calc
  Scenario Outline: Calculate a new distance From [<from>] - To [<to>] with Threads [<threads>] - Positive
#Load new network
    Given I load the REST request {LoadNetwork.json} with profile {load_3_edges}
    Then I make a request to Load New Networks with Vereign Djistra api
    Then the status code should be {201}
    Then I clear the request body
#Calculate distances
    Given I set the request fields
      | from    | <from>    |
      | to      | <to>      |
      | threads | <threads> |
    Then I make a request to calculate a new distance with Vereign Djistra api
    Then the status code should be {201}
    And the response is valid according to the {VereignDjistra.json} REST schema
    And the field {$.result} has the value {<result>}
    And the field {$.errors} has the value {<errors>}
    Examples:
      | from | to | threads | result   | errors |
      | 1    | 2  | 1       | 1.000000 | none   |
      | 0    | 1  | 1       | 1.000000 | none   |
      | 0    | 2  | 1       | 2.000000 | none   |
      | 0    | 0  | 1       | 0.000000 | none   |

  @calc
  Scenario: Calculate a random distances From 0-3 To 8-10 - Positive
#Load new network
    Given I load the REST request {LoadNetwork.json} with profile {load_11_nodes_15_edges}
    Then I make a request to Load New Networks with Vereign Djistra api
    Then the status code should be {201}
    Then I clear the request body
#Calculate distances
    Then I choose a random destinations for From min node {0} max node {3} and To min node {8} max node {10} and calculate the distance with Vereign Djistra api
    Then the status code should be {201}
    And the response is valid according to the {VereignDjistra.json} REST schema
    #TODO validate the answer based on the random From - To distances
   # And the field {$.result} has the value {<result>}
    And the field {$.errors} has the value {none}

  ##cURL of this test returns : curl: (52) Empty reply from server
  ##since the invalid tests are not handled at the moment this negative tests shall be commented out with tag @wip (work in progress)
  ##This is how some of the negative tests may look like
  @calc @negative @wip
  Scenario Outline: Try to calculate a new distance with invalid data From [<from>] - To [<to>] with Threads [<threads>] - Negative
#Load new network
    Given I load the REST request {LoadNetwork.json} with profile {load_3_edges}
    Then I make a request to Load New Networks with Vereign Djistra api
    Then the status code should be {201}
    Then I clear the request body
#Calculate distances
    Given I set the request fields
      | from    | <from>    |
      | to      | <to>      |
      | threads | <threads> |
    Then I make a request to calculate a new distance with Vereign Djistra api
    Then the status code should be {400}
    And the field {$.result} has the value {<result>}
    And the field {$.errors} has the value {<errors>}
    Examples:
      | from | to | threads | result | errors |
      | -1   | 2  | 1       | ???    | ???    |
      | 0    | 4  | 1       | ???    | ???    |