package api.test.rest.l10ns.Receipt;

public class ReceiptsL10ns {

    public String getCustomerCopy(String language) {
        String copy;
        switch (language) {
            case "de":
                copy = "Kundenbeleg";
                break;
            case "en":
                copy = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Customer Copy: " + language);
        }
        return copy;
    }

    public String getMerchantCopy(String language) {
        String copy;
        switch (language) {
            case "de":
                copy = "Händlerkopie";
                break;
            case "en":
                copy = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Merchant Copy: " + language);
        }
        return copy;
    }

    public String getCountry(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Deutschland";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Country: " + language);
        }
        return text;
    }

    public String getMerchantCode(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Händler-ID:";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Merchant Code: " + language);
        }
        return text;
    }

    public String getMerchantVatId(String language) {
        String text;
        switch (language) {
            case "de":
                text = "USt-ID:";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Vat Id: " + language);
        }
        return text;
    }

    public String getTransactionId(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Transaktion-ID:";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Transaction Id: " + language);
        }
        return text;
    }

    public String getTerminalId(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Terminal-ID:";
                break;
            case "en":
                text = "Terminal-ID:";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Terminal Id: " + language);
        }
        return text;
    }


    public String getReceiptNumber(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Beleg-Nr.:";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Receipt Number: " + language);
        }
        return text;
    }

    public String getReceiptSaleNumber(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Verkauf Beleg-Nr";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Receipt Sale Number: " + language);
        }
        return text;
    }

    public String getReceiptRefundNumber(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Beleg-Nr.:";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Receipt Refund Number: " + language);
        }
        return text;
    }

    public String getSaleHeader(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Verkauf";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Sale Header: " + language);
        }
        return text;
    }

    public String getRefundHeader(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Rückbuchung";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Refund Header: " + language);
        }
        return text;
    }

    public String getSaleWithoutTip(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Betrag";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Sale Without Tip: " + language);
        }
        return text;
    }

    public String getSaleWithoutTipTotalAmount(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Gesamtbetrag";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Sale Without Tip Total Amount: " + language);
        }
        return text;
    }

    public String getProductHeader(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Beschreibung";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Product Header: " + language);
        }
        return text;
    }

    public String getNet(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Netto";
                break;
            case "en":
                text = "Net";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Net: " + language);
        }
        return text;
    }

    public String getVat(String language) {
        String text;
        switch (language) {
            case "de":
                text = "MwSt.";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Vat: " + language);
        }
        return text;
    }

    public String getGross(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Brutto";
                break;
            case "en":
                text = "Gross";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Gross: " + language);
        }
        return text;
    }

    public String getPaidStatement(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Gesamtbetrag in Bar bezahlt.";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Paid Statement: " + language);
        }
        return text;
    }

    public String getReminder(String language) {
        String text;
        switch (language) {
            case "de":
                text = "BITTE BEWAHREN SIE DEN BELEG AUF";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Reminder: " + language);
        }
        return text;
    }

    public String getAuthorizationCode(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Autorisierungscode:";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Authorization Code: " + language);
        }
        return text;
    }

    public String getEntryModeManualEntry(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Manuelle Eingabe";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Entry Mode: " + language);
        }
        return text;
    }

    public String getEntryModeChip(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Chip";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Entry Mode: " + language);
        }
        return text;
    }

    public String getEntryModeContactless(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Contactless";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Entry Mode: " + language);
        }
        return text;
    }

    public String getVerificationText(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Zahlung ohne Kartenterminal";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Verification Text: " + language);
        }
        return text;
    }

    public String getDeclaration(String language) {
        String text;
        switch (language) {
            case "de":
                text = "ICH BESTÄTIGE DEN OBEN GENANNTEN " +
                        "GESAMTBETRAG ENTSPRECHEND DER " +
                        "VEREINBARUNG DES KARTENHERAUSGEBERS " +
                        "ZU ZAHLEN.";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Declaration: " + language);
        }
        return text;
    }

    public String getSVGFooter(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Diese Zahlung wurde mit SumUp " +
                        "durchgeführt. Akzeptieren auch Sie " +
                        "Kartenzahlungen mit Ihrem iPhone/ " +
                        "iPad oder Android Smartphone/ " +
                        "Tablet. Weitere Informationen auf " +
                        "sumup.de.";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Footer: " + language);
        }
        return text;
    }

    public String getHTMLFooter(String language) {
        String text;
        switch (language) {
            case "de":
                text = "Diese Zahlung wurde mit SumUp " +
                        "durchgeführt. Akzeptieren auch Sie " +
                        "Kartenzahlungen mit Ihrem iPhone/" +
                        "iPad oder Android Smartphone/" +
                        "Tablet. Weitere Informationen auf " +
                        "sumup.de.";
                break;
            case "en":
                text = "";
                break;
            default:
                throw new IllegalArgumentException("Invalid language for Footer: " + language);
        }
        return text;
    }
}
