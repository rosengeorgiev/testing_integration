package core;

import com.google.common.collect.Iterators;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import core.*;
import utils.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Collection;
import java.util.Random;

/**
 * Created by os-veselin.petrov on 07/06/2017.
 */
public class Tools {

    private static final Logger logger = LogManager.getLogger(Tools.class.getSimpleName());
    private static Gson gson = new Gson();

    /**
     * Loads the JSON body of the provided Request as com.google.gson.JsonObject and returns it
     *
     * @param reqParam the Request object who's body will be loaded and returned
     * @return the loaded body as JsonObject
     */
    public static JsonObject getReqestBodyAsJsonObject(Request reqParam) {

        JsonElement jsonElement = gson.fromJson(reqParam.getBody(), JsonElement.class);

        // only try to load body as JsonObject if it is not null.
        JsonObject jsonObject = new JsonObject();
        if (jsonElement != null) jsonObject = jsonElement.getAsJsonObject();

        return jsonObject;
    }

    public static String jsonObjectToString(JsonObject jsonObject) {

        return gson.toJson(jsonObject);
    }

    /**
     * Returns one of the elments in the provided cllection on random basis.
     *
     * @param inputList The collection of elements to pick-up from
     * @param <T>
     * @return the randomly picked-up element
     */
    public static <T> T getRandomValueFromCollection(Collection<T> inputList) {
        if (inputList.isEmpty()) {
            return null;
        }
        int arrayPosition = (inputList.size() > 1) ? (new Random(System.currentTimeMillis())).nextInt(inputList.size() - 1) : 0;
        System.out.println("DEBUG: arrayPosition = " + arrayPosition);
        return Iterators.get(inputList.iterator(), arrayPosition);

    }

    /**
     * Generates random IPv4 IP address and returns it as string.
     *
     * @return The generated IP
     */
    public static String getRandomIpv4Ip() {
        StringBuffer sb = new StringBuffer(50);
        int segment;
        for (int i = 0; i < 4; i++) {
            segment = RAFRandomize.getInt(0, 255);
            sb.append(segment).append('.');
        }

        sb.deleteCharAt(sb.length() - 1);

        String ipToReturn = sb.toString();
        logger.info("Randomly generated IP: [{}]", ipToReturn);
        return ipToReturn;
    }
}
