package core;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

/**
 * Created by os-veselin.petrov on 18/05/2016.
 */
public class EnvUtils {
    /*******************************************----------------------========================------------------****************************************************
     RA cookies:
     24/01/2017: !! Seems not to be valid anymore !!! : LUX = A8UJK_jrLEA_JrfB-UhBlJoSOo6sM2KfL2NPX270e8-XGJWsHF_K-VuUCLttLE3uq8PZ5ELZYPHvi0LBFy68GxAeF0y_XZGwH_c~
     ZID/STG = AwO-7ioIF9OmT_GX_0er17U9azpz2x6wPN_V6VGTzHCfwboC0KucN8NJZapGwHij-ex2_INZAd_iHbLOjmPPamksU6ccjP1cjXT7lesfgpA1UdqqqO06AodxlbLIKhOUtmAxJmvTOPQ7jctRjGMi1YzQhUvIguUi52AFJSplLGi8nA~~
     ******************************************----------------------========================------------------*****************************************************/
    public static final String DEFAULT_RA_COOKIE = "AwO-7ioIF9OmT_GX_0er17U9azpz2x6wPN_V6VGTzHCfwboC0KucN8NJZapGwHij-ex2_INZAd_iHbLOjmPPamksU6ccjP1cjXT7lesfgpA1UdqqqO06AodxlbLIKhOUtmAxJmvTOPQ7jctRjGMi1YzQhUvIguUi52AFJSplLGi8nA~~";
    private static final Logger logger = LogManager.getLogger(EnvUtils.class.getSimpleName());

    /**
     * Finds a local, non-loopback, IPv4 address
     *
     * @return String representation of the first IPv4 address found OR "127.0.0.1" if such was not found.
     */
    public static String getIp() {
        String ip = "127.0.0.1";
        String hostName = "127.0.0.1";

        try {
            Enumeration niE = NetworkInterface.getNetworkInterfaces();
            while (niE.hasMoreElements()) {
                NetworkInterface n = (NetworkInterface) niE.nextElement();
                Enumeration inaE = n.getInetAddresses();
                while (inaE.hasMoreElements()) {
                    InetAddress inetAddr = (InetAddress) inaE.nextElement();

                    if (inetAddr instanceof Inet4Address && !inetAddr.isLoopbackAddress()) {
                        ip = inetAddr.getHostAddress();
                        hostName = inetAddr.getHostName();
                        break;
                    }

                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }

        logger.info("Get IP addr. of the machine..... IP= {} ; Host= {}", ip, hostName);

        return ip;
    }

    public static String getRaCookie() {
        //get raCookie value if provided by build gradle parameter:
        if (!System.getProperty("raCookie").equals("null")) {

            return System.getProperty("raCookie");

        } else { //else print message it is not provided as build param and fall back to the default STG value:

            logger.info("'raCookie' build param is not provided. Falling back to the default value= {}", DEFAULT_RA_COOKIE);
            return DEFAULT_RA_COOKIE;

        }

    }

    public static List<String> getAllIPs() {
        List<String> obtainedIPs = new ArrayList<>();

        try {
            Enumeration niE = NetworkInterface.getNetworkInterfaces();
            while (niE.hasMoreElements()) {
                NetworkInterface n = (NetworkInterface) niE.nextElement();
                Enumeration inaE = n.getInetAddresses();
                while (inaE.hasMoreElements()) {
                    InetAddress inetAddr = (InetAddress) inaE.nextElement();
                    if (!inetAddr.isLoopbackAddress()) {
                        obtainedIPs.add(inetAddr.getHostAddress());
                    }

                }
            }
        } catch (SocketException e) {
            e.printStackTrace();
        }

        logger.debug("Obtained machine IP addresses: {}", obtainedIPs);

        return obtainedIPs;

    }
}
