package selenium.utils;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;
import org.openqa.selenium.winium.WiniumDriverService;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;


public class Browser {

  public static WebDriver driver;
  public static WiniumDriver winDriver;
  public static HtmlUnitDriver HTMLdriver;
  public static WiniumDriverService service;

    public static void initHTMLUnitDriver() {
        HTMLdriver = new HtmlUnitDriver();
      //  HTMLdriver.setProxy();
    }

    public static void initFF() throws InterruptedException {
        FirefoxProfile profile = new FirefoxProfile();
        profile.setPreference(FirefoxProfile.ALLOWED_HOSTS_PREFERENCE, "localhost");
        profile.setPreference(FirefoxProfile.ALLOWED_HOSTS_PREFERENCE, "localhost.localdomain");
        profile.setPreference("browser.privatebrowsing.autostart", true);
        driver = new FirefoxDriver(profile);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    public static void initWindows() throws IOException {

        String winiumDriverPath = System.getProperty("winiumDriverPath");

        DesktopOptions options = new DesktopOptions();
        options.setApplicationPath(System.getProperty("windowsAppLocation"));
        options.setLaunchDelay(1000);

        File drivePath = new File(winiumDriverPath); //Set winium driver path
        service = new WiniumDriverService.Builder().usingDriverExecutable(drivePath).usingPort(9999).withVerbose(true).withSilent(false).buildDesktopService();
        service.start();

        winDriver = new WiniumDriver(service, options);
    }

    public static void initChrome() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", System.getProperty("chromeDriver"));
        ChromeOptions options = new ChromeOptions();
        options.addArguments("--no-experiments");
        options.addArguments("--disable-translate");
        options.addArguments("--disable-plugins");
        options.addArguments("--disable-extensions");
        options.addArguments("--no-default-browser-check");
        options.addArguments("--clear-token-service");
        options.addArguments("--disable-default-apps");
        options.addArguments("--enable-logging");
        options.addArguments("--dns-prefetch-disable");
        options.addArguments("--start-maximized");
        DesiredCapabilities dc = DesiredCapabilities.chrome();
        dc.setCapability(ChromeOptions.CAPABILITY, options);
        driver = new ChromeDriver(dc);
        TimeUnit.SECONDS.sleep(1);
    }

    public static void goTo(String url) {
        Browser.driver.get(url);
        WaitTool.setImplicitWait(Browser.driver, 5);
    }

    public static String getUrl() throws Throwable {
        String url = Browser.driver.getCurrentUrl();
        return url;
    }

    public static void tearDown() throws InterruptedException, IOException {
        driver.quit();
    }

    public static void windowsDriverTearDown() throws InterruptedException, IOException {
        winDriver.quit();
        service.stop();
    }
}
