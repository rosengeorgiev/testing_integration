package selenium.utils;

import java.util.Set;


public class ChangeWindow {

    public static void handleMultipleWindows(String windowTitle) {
        Set<String> windows = Browser.driver.getWindowHandles();

        for (String window : windows) {
            Browser.driver.switchTo().window(window);
            if (Browser.driver.getTitle().contains(windowTitle)) {
                return;
            }
        }
    }
}
