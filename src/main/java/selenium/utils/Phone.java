package selenium.utils;

import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;
import org.openqa.selenium.remote.DesiredCapabilities;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.html5.Location;


public class Phone {

  public static AndroidDriver driver;

    public static void initAndroidDriver() throws MalformedURLException {
      File classpathRoot = new File(System.getProperty("user.dir"));
      File appDir = new File(classpathRoot, "testapps");
      File app = new File(appDir, "");

      DesiredCapabilities capabilities = new DesiredCapabilities();
      capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, MobilePlatform.ANDROID);
      capabilities.setCapability(MobileCapabilityType.PLATFORM_VERSION, "7.1.1");
      capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "Android");
      capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
      capabilities.setCapability("appPackage", "com.kaching.merchant.beta");
      capabilities.setCapability("appActivity", "com.kaching.merchant.Activities.RegisterActivity");
      capabilities.setCapability("locationServicesAuthorized", true);
      capabilities.setCapability("locationServicesEnabled", true);

      driver = new AndroidDriver<MobileElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities);

      // Setting mock location
      Location loc = new Location(20.0, 12.5, 1000);  // latitude, longitude, altitude
      driver.setLocation(loc);
    }

    public static void tearDown() throws InterruptedException, IOException {
      driver.closeApp();
      driver.quit();
    }
}
