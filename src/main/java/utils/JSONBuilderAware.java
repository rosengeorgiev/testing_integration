package utils;

/**
 * Allows to build complex JSON strings with any objects which implements this interface
 */
public interface JSONBuilderAware {
    JSONBuilder addItem();
}
