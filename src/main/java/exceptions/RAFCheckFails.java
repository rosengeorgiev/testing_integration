package exceptions;

/**
 * Used when there no actual problem but just some test check fails
 */
public class RAFCheckFails extends RAFException {

    private static final long serialVersionUID = 8400876084535941076L;

    public RAFCheckFails(String errorMsg, Class clazz) {
        super(errorMsg, clazz);
    }

}
